const supertest = require('supertest');
const app = require('../app');

//sentencia

describe("Probar el sistema de auticacion",()=> {
    it("deberia de obtener un login con usuario y contraseña correctos",(done)=> {
        supertest(app).get("/copies/").send({'email':'jose@uach.com','password':'hola'}).expect(200).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).get("/copies/id").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).post("/copies/").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).put("/copies/id").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).patch("/copies/id").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).delete("/copies/id").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });
});