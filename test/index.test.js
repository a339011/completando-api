const supertest = require('supertest');
const app = require('../app');

//sentencia

describe("Probar el sistema de auticacion",()=> {
    it("deberia de obtener un login con usuario y contraseña correctos",(done)=> {
        supertest(app).post("/login").send({'email':'jose@uach.com','password':'hola'}).expect(200).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });

    it("deberia de obtener un login con usuario y contraseña incorrectos",(done)=> {
        supertest(app).post("/login").send({'email':'jose@uach.com','password':'hol'}).expect(403).end(function(err,res){
            if(err){
                done(err);

            }else{
                done();
            }
        })
    });
});