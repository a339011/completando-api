const express = require('express');
const Member = require('../models/member');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function list(req, res, next) {
    Member.find().then(objs => {res.status(200).json({
        message:res.__('list.member'),
        obj: objs
    })
    logger.debug(`respond with list`);
    }).catch(ex => {res.status(500).json({
        message:res.__('list.no'),
        obj:ex
    })
    logger.debug(`respond with error`);
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Member.findOne({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('index.member'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message:res.__('index.no'),
        obj: ex
    })
    logger.debug(`respond with error`);
    });
}

function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    
    let address = new Object();
    address.street = req.body.street;
    address.number = req.body.number;
    address.zip = req.body.zip;
    address.state = req.body.state;
    
    
    const phone = req.body.phone;

    let member = new Member({
        name,
        lastName,
        phone,
        address,
    });

    member.save().then(obj => {res.status(200).json({
        message:res.__('create.member'),
        obj: obj
    })
    logger.debug('respond with create');
    }).catch(ex => {res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    })
    logger.debug('respond with error');
    });
}

function replace(req, res, next) {
    const id= req.params.id;

    const name = req.body.name ? req.body.name : "";
    const lastName = req.body.lastName ? req.body.lastName : "";
    let address = new Object();
    address.street = req.body.street ? req.body.street : "";
    address.number = req.body.number ? req.body.number : "";
    address.zip = req.body.zip ? req.body.zip : 0;
    address.state = req.body.state ? req.body.state : "";
    const phone = req.body.phone ? req.body.phone : "";

    let member = new Object({
        _name:name,
        _lastName: lastName,
        _phone: phone,
        _address: address,
    });
    console.log(member);

    Member.findOneAndUpdate({"_id":id}, member, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.member'),
        obj:obj
    }),
    logger.debug('respond with replace');
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;

    const name = req.body.name;
    const lastName = req.body.lastName;
    
    const street = req.body.street;
    const number = req.body.number;
    const zip = req.body.zip;
    const state = req.body.state;
    
    const phone = req.body.phone;
    
    let member = new Object();

    let address = new Object();

    if(name){
        member._name=name;
    }

    if(lastName){
        member._lastName=lastName;
    }

    if(street){
        address.street=street;
    }

    if(number){
        address.number=number;
    }

    if(zip){
        address.zip=zip;
    }

    if(state){
        address.state=state;
    }

    member._address=address;

    if(phone){
        member._phone=phone;
    }


    Member.findOneAndUpdate({"_id":id}, member,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.member'),
        obj:obj
    }),
    logger.debug(`respond with update`);
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    }),
    logger.debug('respond with error');
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Member.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.member'),
        obj:obj
    }),
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    }),
    logger.debug(`respond with error`);
});
}


module.exports = { list, index, create, replace, update, destroy };