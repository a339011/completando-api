//checado
const express = require('express');
const Actor = require('../models/actor');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function list(req, res, next) {
    Actor.find().then(
        objs => { 
        res.status(200).json({
        message: res.__('list.actor'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex => {
        res.status(500).json({
        message: res.__('list.no'),
        obj:ex
    }),
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Actor.findOne({"_id":id}).then(obj =>{ res.status(200).json({
        message:res.__('index.actor'),
        obj:obj
    }),
    logger.debug(`respond with index >>${id}`);
    }).catch(ex =>{ res.status(500).json({
        message: res.__('index.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}


function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let actors = new Actor({
        name: name,
        lastName:lastName
    });

    actors.save().then(obj =>{ res.status(200).json({
        message:res.__('create.actor'),
        obj: obj
    });
    logger.debug("respond with create");
    }).catch(ex => {res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });    
}

function replace(req, res, next) {
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";

    let actor = new Object({
        _name:name,
        _lastName:lastName
    });

    Actor.findOneAndUpdate({"_id":id}, actor, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.actor'),
        obj:obj
    }),
    logger.debug("respond with replace");
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let actor = new Object();

    if(name){
        actor._name=name;
    }

    if(lastName){
        actor._lastName=lastName;
    }

    Actor.findOneAndUpdate({"_id":id}, actor,{new : true}).then(obj =>{ res.status(200).json({
        message:res.__('update.actor'),
        obj:obj
    }),
    logger.debug("respond with update");
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    }),
    logger.debug("respond with error");
});
}

function destroy(req, res, next) {
    const id = req.params.id;
    Actor.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.actor'),
        obj:obj
    })
    logger.debug("respond with delete");
    }).catch(ex =>{ res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    }),
    logger.debug("respond with error");
});
}


module.exports = { list, index, create, replace, update, destroy };