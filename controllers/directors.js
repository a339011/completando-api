//checado
const express = require('express');
const Director = require('../models/director');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';


function list(req, res, next) {
    Director.find().then(objs => {res.status(200).json({
        message:res.__('list.director'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex => {res.status(500).json({
        message:res.__('list.no'),
        obj:ex
    }),
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Director.findOne({"_id":id}).then(obj =>{ res.status(200).json({
        message:res.__('index.director'),
        obj:obj
    }),
    logger.debug(`respond with index >>${id}`);
    }).catch(ex =>{ res.status(500).json({
        message:res.__('index.no'),
        obj: ex
    }),
    logger.debug("respond with error");
});
}


function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;

    let director = new Director({
        name: name,
        lastName:lastName
    });

    director.save().then(obj => {res.status(200).json({
        message:res.__('create.director'),
        obj: obj
    })
    logger.debug(`respond with index create`);
    }).catch(ex =>{ res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    })
    logger.debug("respond with error");
    }
    );
}

function replace(req, res, next) {
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";

    let director = new Object({
        _name:name,
        _lastName:lastName
    });

    Director.findOneAndUpdate({"_id":id}, director, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.director'),
        obj:obj
    })
    logger.debug(`respond with replace`);
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;

    let director = new Object();

    if(name){
        director._name=name;
    }

    if(lastName){
        director._lastName=lastName;
    }

    Director.findOneAndUpdate({"_id":id}, director,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.director'),
        obj:obj
    })
    logger.debug(`respond with update`);
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Director.remove({"_id":id}).then(obj => {
        res.status(200).json({
        message:res.__('delete.director'),
        obj:obj
    })
    logger.debug(`respond with delete`);
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };