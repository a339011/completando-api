const express = require('express');
const Movie = require('../models/movie');
const Genre = require('../models/genre');
const Director = require('../models/director');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';


function list(req, res, next) {
    Movie.find().then(objs => {res.status(200).json({
        message:res.__('list.movie'),
        obj: objs
    })
    logger.debug(`respond with list`);
    }).catch(ex => {res.status(500).json({
        message:res.__('list.no'),
        obj:ex
    })
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Movie.findOne({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('index.movie'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message:res.__('index.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


async function create(req, res, next) {
    const title = req.body.title;
    const genreId = req.body.genreId;
    const directorId = req.body.directorId;

    let genre = await Genre.findOne({"_id": genreId});

    let movie = new Movie({
        title: title,
        genre:genre
    });

    movie.save().then(obj => {res.status(200).json({
        message:res.__('create.movie'),
        obj: obj
    })
    logger.debug(`respond with create`);
    }).catch(ex => {res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let title = req.body.title ? req.body.title : "";
    let genreId = req.body.genreId ? req.body.genreId : false;

    let genre = new Object({
        _title:title,
        _genreId:genreId
    });

    Movie.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.movie'),
        obj:obj
    })
    logger.debug(`respond with replace`);
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let title = req.body.title;
    let genreId = req.body.genreId;

    let genre = new Object();

    if(title){
        genre._title=title;
    }

    if(genreId){
        genre._genreId=genreId;
    }

    Movie.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.movie'),
        obj:obj
    })
    logger.debug(`respond with update`);  
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Movie.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.movie'),
        obj:obj
    })
    logger.debug(`respond with delete`);
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };