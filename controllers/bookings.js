const express = require('express');
const Booking = require('../models/booking');
const Copy = require('../models/booking');
const Member = require('../models/booking');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function list(req, res, next) {
    Booking.find().then(objs =>{ res.status(200).json({
        message: res.__('list.booking'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex =>{ res.status(500).json({
        message: res.__('list.no'),
        obj:ex
    }),
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Booking.findOne({"_id":id}).then(obj => {res.status(200).json({
        message: res.__('index.booking'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message: res.__('index.no'),
        obj: ex
    })
    logger.debug('respond with error ');
    });
}


async function create(req, res, next) {
    const date = req.body.date;
    const memberId = req.body.memberId;
    const copyId = req.body.copyId;

    let member = await Member.findOne({"_id": memberId});
    let copy = await Copy.findOne({"_id": copyId});

    let booking = new Booking({
        date: date,
        member:member,
        copy:copy
    });

    booking.save().then(obj => {res.status(200).json({
        message: res.__('create.booking'),
        obj: obj
    })
    logger.debug('respond with create');
    }).catch(ex => {res.status(500).json({
        message: res.__('create.no'),
        obj: ex
    })
    logger.debug('respond with error');
    });

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let date = req.body.date ? req.body.date : "";
    let memberId = req.body.memberId ? req.body.memberId : false;

    let booking = new Object({
        _date:date,
        _memberId:memberId
    });

    Booking.findOneAndUpdate({"_id":id}, booking, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.booking'),
        obj:obj
    }),
    logger.debug('respond with replace');
    }).catch(ex =>{ res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug('respond with error');
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let date = req.body.date;
    let memberId = req.body.memberId;

    let booking = new Object();

    if(date){
        booking._date=date;
    }

    if(memberId){
        booking._memberId=memberId;
    }

    Booking.findOneAndUpdate({"_id":id}, booking,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.booking'),
        obj:obj
    }),
    logger.debug('respond with update');
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    }),
    logger.debug('respond with error');
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Booking.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.booking'),
        obj:obj
    })
    logger.debug('respond with delete');
    }).catch(ex =>{ res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug('respond with error');
    });
}


module.exports = { list, index, create, replace, update, destroy };