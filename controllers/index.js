var express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const config = require('config');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function home(req, res, next) {
    res.render('index', { title: 'Express' });
}

function login(req,res,next){
    const email = req.body.email;
    const password = req.body.password;

    User.findOne({"_email":email}).select('_password _salt')
    .then((user)=>{
        if(user){
            bcrypt.hash(password, user.salt, (err, hash)=>{
                if(err){
                    res.status(403).json({
                        message:res.__('bad.login'),
                        obj:err
                    });
                    logger.debug("respond with error");
                }
                if(hash === user.password){
                    const jwtKey = config.get("secret.key");
                    res.status(200).json({
                        message:res.__('ok.login'),
                        obj:jwt.sign({exp: Math.floor(Date.now()/1000)+60}, jwtKey)
                    });
                    logger.debug("respond with token");
                }else{
                    res.status(403).json({
                        message:res.__('bad.login'),
                        obj:null,
                    });
                    logger.debug("respond with error");
                }
            });

        }else{
            res.status(403).json({
                message:res.__('bad.login'),
                obj:null,
            });
            logger.debug("respond with error");
        }
    }).catch(err => {
        res.status(403).json({
            message:res.__('bad.login'),
            obj:null,
        });
        logger.debug("respond with error");
    });
}

module.exports = { home, login };