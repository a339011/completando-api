const express = require('express');
const { format } = require('morgan');
const Copy = require('../models/movie');
const Movie = require('../models/movie');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';


function list(req, res, next) {
    Copy.find().then(objs => {res.status(200).json({
        message: res.__('list.copy'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex =>{ res.status(500).json({
        message: res.__('list.no'),
        obj:ex
    }),
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Copy.findOne({"_id":id}).then(obj => {res.status(200).json({
        message: res.__('index.copy'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message: res.__('index.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}


async function create(req, res, next) {
    const number = req.body.number;
    const movieId = req.body.movieId;
    const format = req.body.format;
    const status = req.body.status;

    let movie = await Movie.findOne({"_id": movieId});

    let copy = new Copy({
        number: number,
        format: format,
        movie:movie,
        status:status,
    });

    copy.save().then(obj => {res.status(200).json({
        message: res.__('create.copy'),
        obj: obj
    })
    logger.debug("respond with create");
    }).catch(ex => {res.status(500).json({
        message: res.__('create.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let title = req.body.title ? req.body.title : "";
    let movieId = req.body.movieId ? req.body.movieId : false;

    let movie = new Object({
        _title:title,
        _movieId:movieId
    });

    Copy.findOneAndUpdate({"_id":id}, movie, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.copy'),
        obj:obj
    })
    logger.debug("respond with replace");
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let title = req.body.title;
    let movieId = req.body.movieId;

    let movie = new Object();

    if(title){
        movie._title=title;
    }

    if(movieId){
        movie._movieId=movieId;
    }

    Copy.findOneAndUpdate({"_id":id}, movie,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.copy'),
        obj:obj
    })
    logger.debug("respond with update");
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.copy'),
        obj:obj
    })
    logger.debug("respond with delete");
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    }),
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };