const express = require('express');
const AwaitList = require('../models/await_list');
const Movie = require('../models/movie');
const Member = require('../models/member');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function list(req, res, next) {
    AwaitList.find().then(objs => {res.status(200).json({
        message: res.__('list.al'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex => {res.status(500).json({
        message: res.__('list.no'),
        obj:ex
    }),
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    AwaitList.findOne({"_id":id}).then(obj => {res.status(200).json({
        message: res.__('index.al'),
        obj:obj
    }),
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message: res.__('index.no'),
        obj: ex
    }),
    logger.debug('respond with error');
    });
}


async function create(req, res, next) {
    const movieId = req.body.movieId;
    const memberId = req.body.memberId;

    let movie = await Movie.findOne({"_id": movieId});

    let member = await Member.findOne({"_id": memberId});

    let await_list = new AwaitList({
        movie: movie,
        member: member
    });

    await_list.save().then(obj =>{ res.status(200).json({
        message: res.__('create.al'),
        obj: obj
    }),
    logger.debug("respond with create");
    }).catch(ex =>{ res.status(500).json({
        message: res.__('create.no'),
        obj: ex
    }),
    logger.debug("respond with error");
});
}

async function replace(req, res, next) {
    const id= req.params.id;
    let movieId = req.body.movieId ? req.body.movieId : null;
    const memberId = req.body.memberId ? req.body.memberId : null;

    let movie = await Movie.findOne({"_id": movieId});

    let member = await Member.findOne({"_id": memberId});

    let await_list = new Object({
        movie: movie,
        member: member
    });

    AwaitList.findOneAndUpdate({"_id":id}, await_list, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.al'),
        obj:obj
    }),
    logger.debug('respond with replace');
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug('respond with error');
    });
}

async function update(req, res, next) {
    const id= req.params.id;
    let movieId = req.body.movieId;
    let memberId = req.body.memberId;

    let await_list = new Object();

    if(movieId){
        await_list._movieId= movieId;
    }

    if(memberId){
        await_list._memberId = memberId;
    }

    AwaitList.findOneAndUpdate({"_id":id}, await_list, {new : true}).then(obj => {res.status(200).json({
        message:res.__('update.al'),
        obj:obj
    }),
    logger.debug("respond with update");
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    AwaitList.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.al'),
        obj:obj
    }),
    logger.debug("respond with delete");
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };