
const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';

function list(req, res, next) {
    User.find().then(
        objs => { 
        res.status(200).json({
        message:res.__('list.usuarios'),
        obj: objs
    }),
    logger.debug("respond with list");
    }).catch(ex => {
        res.status(500).json({
        message: res.__('list.no'),
        obj:ex
    })
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    User.findOne({"_id":id}).then(obj =>{ res.status(200).json({
        message:res.__('index.usuarios'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex =>{ res.status(500).json({
        message: res.__('index.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


async function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    const salt = await bcrypt.genSalt(10);

    const passwordHash = await bcrypt.hash(password, salt);

    let user = new User({
        name: name,
        lastName: lastName,
        email : email,
        password : passwordHash,
        salt : salt,
    });

    user.save().then(obj => {res.status(200).json({
        message:res.__('create.usuarios'),
        obj: obj
    }),
    logger.debug("respond with create");
    }).catch(ex => {res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

async function replace(req, res, next) {
    const id= req.params.id;
    let name = req.body.name ? req.body.name : "";
    let lastName = req.body.lastName ? req.body.lastName : "";
    let email = req.body.email  ? req.body.email : "";
    let password = req.body.password ? req.body.password : "";

    const salt = await bcrypt.genSalt(10);

    const passwordHash = await bcrypt.hash(password, salt);

    let user = new Object({
        _name: name,
        _lastName: lastName,
        _email : email,
        _password : passwordHash,
        _salt : salt,
    });

    User.findOneAndUpdate({"_id":id}, user, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.usuarios'),
        obj:obj
    }),
    logger.debug("respond with replace");
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

async function update(req, res, next) {
    const id= req.params.id;
    let name = req.body.name;
    let lastName = req.body.lastName;
    let email = req.body.email ;
    let password = req.body.password;

    const salt = await bcrypt.genSalt(10);

    const passwordHash = await bcrypt.hash(password, salt);

    let user = new Object();

    if(name){
        user._name=name;
    }

    if(lastName){
        user._lastName=lastName;
    }

    if(email){
        user._email=email;
    }

    if(password){
        user._password=passwordHash;
        user._salt=salt;
    }

    User.findOneAndUpdate({"_id":id}, user, {new : true}).then(obj =>{ res.status(200).json({
        message:res.__('update.usuarios'),
        obj:obj
    })
    logger.debug("respond with update");
    }).catch(ex =>{ res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    })
    logger.debug("respond with error");
});
}

function destroy(req, res, next) {
    const id = req.params.id;
    User.remove({"_id":id}).then(obj =>{ res.status(200).json({
        message:res.__('delete.usuarios'),
        obj:obj
    })
    logger.debug("respond with destroy");
    }).catch(ex =>{ res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };