//checado
const express = require('express');
const Genre = require('../models/genre');
const log4js = require('log4js');
const logger = log4js.getLogger();
logger.level = 'debug';


function list(req, res, next) {
    Genre.find().then(objs => {res.status(200).json({
        message:res.__('list.genre'),
        obj: objs
    })
    logger.debug(`respond with list`);
    }).catch(ex => {res.status(500).json({
        message:res.__('list.no'),
        obj:ex
    })
    logger.debug("respond with error");
    });
}

function index(req, res, next) {
    const id = req.params.id;
    Genre.findOne({"_id":id}).then(obj =>{ res.status(200).json({
        message:res.__('index.genre'),
        obj:obj
    })
    logger.debug(`respond with index >>${id}`);
    }).catch(ex => {res.status(500).json({
        message:res.__('index.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


function create(req, res, next) {
    const description = req.body.description;
    const status = req.body.status;

    let genre = new Genre({
        description: description,
        status:status
    });

    genre.save().then(obj => {res.status(200).json({
        message:res.__('create.genre'),
        obj: obj
    })
    logger.debug(`respond with create`);
    }).catch(ex => {res.status(500).json({
        message:res.__('create.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });

    
}

function replace(req, res, next) {
    const id= req.params.id;
    let description = req.body.description ? req.body.description : "";
    let status = req.body.status ? req.body.status : false;

    let genre = new Object({
        _description:description,
        _status:status
    });

    Genre.findOneAndUpdate({"_id":id}, genre, {new : true}).then(obj => {res.status(200).json({
        message:res.__('replace.genre'),
        obj:obj
    })
    logger.debug(`respond with replace`);
    }).catch(ex => {res.status(500).json({
        message:res.__('replace.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function update(req, res, next) {
    const id= req.params.id;
    let description = req.body.description;
    let status = req.body.status;

    let genre = new Object();

    if(description){
        genre._description=description;
    }

    if(status){
        genre._status=status;
    }

    Genre.findOneAndUpdate({"_id":id}, genre,{new : true}).then(obj => {res.status(200).json({
        message:res.__('update.genre'),
        obj:obj
    })
    logger.debug(`respond with update`);
    }).catch(ex => {res.status(500).json({
        message:res.__('update.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}

function destroy(req, res, next) {
    const id = req.params.id;
    Genre.remove({"_id":id}).then(obj => {res.status(200).json({
        message:res.__('delete.genre'),
        obj:obj
    })
    logger.debug(`respond with delete`);
    }).catch(ex => {res.status(500).json({
        message:res.__('delete.no'),
        obj: ex
    })
    logger.debug("respond with error");
    });
}


module.exports = { list, index, create, replace, update, destroy };