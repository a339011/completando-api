const mongoose = require('mongoose');
const Movie = require('./movie');
const Member = require('./member');


const schema = mongoose.Schema({
    _movie: {
        type: mongoose.Schema.ObjectId,
        ref: 'Movie'
    },
    _member: {
        type: mongoose.Schema.ObjectId,
        ref: 'Member'
    }
});

class AwaitList{

    constructor(movie, member){
        this._movie=movie;
        this._member=member;
    }
    get movie(){
        return this._movie;
    }

    set movie(movie){
        this._movie=movie;
    }
    get member(){
        return this.member;
    }

    set member(member){
        this._member=member;
    }
}

schema.loadClass(AwaitList);
module.exports = mongoose.model("AwaitList",schema);