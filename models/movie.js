const mongoose = require('mongoose');
const director = require('./director');

const schema = mongoose.Schema({
    _title: String,
    _genre: {
        type: mongoose.Schema.ObjectId,
        ref: 'Genre'
    }

});

class Movie{

    constructor(title, genre){
        this._title=title;
        this._genre=genre;
    }
    get title(){
        return this._title;
    }

    set title(title){
        this._title=title;
    }
    get genre(){
        return this.genre;
    }

    set genre(genre){
        this._genre=genre;
    }
}

schema.loadClass(Movie);
module.exports = mongoose.model("Movie",schema);